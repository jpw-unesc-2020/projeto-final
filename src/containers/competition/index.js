import React from 'react';
import { Button, Form } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { withRouter } from 'react-router-dom'
import { GET, POST, PUT, STATUS_CODE } from '../../api';
import { parseToField } from '../../utils/date';
import './index.css';

class Competition extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      validated: false,
      formDisabled: false,
      competition: {},
    }
  }

  componentDidMount() {
    const { location } = this.props;
    
    if (location && location.state) {
      GET(`/competition/${location.state}`).then(res => {
        this.setState({ competition: res.data });
      }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
    }
  }

  onChangeInfo = (field, value) => {
    this.setState({ competition: {...this.state.competition, [field]: value} });    
  }

  handleSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    
    const form = event.currentTarget;
    let redirect = true;

    if (form.checkValidity() === false) {
      redirect = false;
    }

    this.setState({ validated: true });

    if (redirect) {
      const { competition } = this.state;

      if (competition._id) {
        PUT(`/competition/${competition._id}`, competition).then(res => {
          if (res.status === STATUS_CODE.OK) {
            this.handleToastSaveSuccess();
          } else {
            this.handleToastSaveError();
          }
        }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
      } else {
        POST(`/competition`, competition).then(res => {
          if (res.status === STATUS_CODE.OK) {
            this.handleToastSaveSuccess();
          } else {
            this.handleToastSaveError();
          }
        }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
      }      
    }
  }

  handleToastSaveSuccess = () => {
    this.setState({ formDisabled: true });
    toast.success('Competição salva com sucesso!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false });
    setTimeout(() => {this.props.history.push('competitions')}, 3000);
  }

  handleToastSaveError = () => {
    toast.error('Erro ao salvar competição!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false });
  }

  render() {
    const { validated, competition, formDisabled } = this.state;

    return (
      <div className="container">        
        <div className="content">
          <div className="title">Competição</div>
          <Form noValidate validated={validated} onSubmit={this.handleSubmit}>
            <div className="small-screen flex-row">
              <Form.Group controlId="uid" className="container-field">
                <Form.Label>UID</Form.Label>
                <Form.Control type="text" disabled value={competition._id}/>
              </Form.Group>
              <Form.Group controlId="name" className="container-field">
                <Form.Label>Nome<span className="required">*</span></Form.Label>
                <Form.Control required type="text" onChange={(e) => this.onChangeInfo('name', e.target.value)} value={competition.name} disabled={formDisabled} />
                <Form.Control.Feedback type="invalid">Nome obrigatório!</Form.Control.Feedback>
              </Form.Group>
            </div>
            <div className="small-screen flex-row">
              <Form.Group controlId="organization" className="container-field">
                <Form.Label>Organização<span className="required">*</span></Form.Label>
                <Form.Control required type="text" onChange={(e) => this.onChangeInfo('organization', e.target.value)} value={competition.organization} disabled={formDisabled}/>
                <Form.Control.Feedback type="invalid">Organização obrigatória!</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="edition" className="container-field">
                <Form.Label>Edição<span className="required">*</span></Form.Label>
                <Form.Control required type="number" onChange={(e) => this.onChangeInfo('edition', e.target.value)} value={competition.edition} disabled={formDisabled}/>
                <Form.Control.Feedback type="invalid">Edição obrigatória!</Form.Control.Feedback>
              </Form.Group>
            </div>
            <div className="small-screen flex-row">
              <Form.Group controlId="date_initial" className="container-field">
                <Form.Label>Dt. Inicial<span className="required">*</span></Form.Label>
                <Form.Control required type="date" onChange={(e) => this.onChangeInfo('date_initial', e.target.value)} value={parseToField(competition.date_initial)} disabled={formDisabled}/>
                <Form.Control.Feedback type="invalid">Dt. Inicial obrigatória!</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="date_end" className="container-field">
                <Form.Label>Dt. Final<span className="required">*</span></Form.Label>
                <Form.Control required type="date" onChange={(e) => this.onChangeInfo('date_end', e.target.value)} value={parseToField(competition.date_end)} disabled={formDisabled}/>
                <Form.Control.Feedback type="invalid">Dt. Final obrigatória!</Form.Control.Feedback>
              </Form.Group>
            </div>
            <div className="small-screen flex-row">
              <Form.Group controlId="location" className="container-field">
                <Form.Label>Localização<span className="required">*</span></Form.Label>
                <Form.Control required type="text" onChange={(e) => this.onChangeInfo('location', e.target.value)} value={competition.location} disabled={formDisabled}/>
                <Form.Control.Feedback type="invalid">Localização obrigatória!</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="number_teams" className="container-field">
                <Form.Label>N° Times<span className="required">*</span></Form.Label>
                <Form.Control required type="number" onChange={(e) => this.onChangeInfo('number_teams', e.target.value)} value={competition.number_teams} disabled={formDisabled}/>
                <Form.Control.Feedback type="invalid">N° de times obrigatório!</Form.Control.Feedback>
              </Form.Group>
            </div>
            <div className="small-screen flex-row">
              <Form.Group controlId="system" className="container-field">
                <Form.Label>Sistema<span className="required">*</span></Form.Label>
                <Form.Control required type="text" onChange={(e) => this.onChangeInfo('system', e.target.value)} value={competition.system} disabled={formDisabled}/>
                <Form.Control.Feedback type="invalid">Sistema obrigatório!</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="premium_value" className="container-field">
                <Form.Label>Premiação<span className="required">*</span></Form.Label>
                <Form.Control required type="number" onChange={(e) => this.onChangeInfo('premium_value', e.target.value)} value={competition.premium_value} disabled={formDisabled}/>
                <Form.Control.Feedback type="invalid">Premiação obrigatória!</Form.Control.Feedback>
              </Form.Group>
            </div>
            <div className="container-button">
              <Button variant="secondary" type="button" disabled={formDisabled} onClick={() => this.props.history.push('competitions')} style={{ marginRight: '15px'}}>Cancelar</Button>
              <Button variant="success" type="submit" disabled={formDisabled}>Salvar</Button>
            </div>
          </Form>  
        </div>
        <ToastContainer />
      </div>
    );
  }
};

export default withRouter(Competition);
