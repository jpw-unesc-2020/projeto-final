import React from 'react';
import { withRouter } from 'react-router-dom';
import { Card, Button, Form, Modal, Spinner } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { BsTrash, BsPencil, BsSearch, BsPlus } from 'react-icons/bs';
import { GET, DELETE, STATUS_CODE } from '../../api';
import { parsePtBr } from '../../utils/date';
import { getBorderFromStatus, getDescriptionFromStatus } from './rules';
import Dialog from '../../components/modal';
import './index.css';

class Games extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      showDialogDelete: false,
      showDialogInfo: false,
      showSpinner: true,
      filterValue: '',
      games: [],
      game: {},
    };
  }

  componentDidMount() {
    this.handleSearchGames();
  }

  handleSearchGames = (url = '') => {
    this.setState({ showSpinner: true });
    GET(`/game${url}`).then(res => {
      this.setState({ games: res.data, showSpinner: false });
    }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
  }

  handleRedirectToGame = (_id) => {
    this.props.history.push('game', (_id ? _id : ''));
  }

  handleDeleteItem = () => {
    const { game } = this.state;

    DELETE(`/game/${game._id}`).then(res => {
      if (res.status === STATUS_CODE.OK) {
        toast.success(`Jogo ${game.team_one.name} X ${game.team_two.name} excluído com sucesso!`, { position: 'top-right', autoClose: 2000, closeOnClick: false, pauseOnHover: false, draggable: false });
        this.setState({ game: {}, showDialogInfo: false, showDialogDelete: false });
        this.handleSearchGames();
      }
    }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
  }

  handleOpenDialogInfo = (game) => {
    this.setState({ game: game, showDialogInfo: true });
  }

  handleCloseDialogInfo = () => {
    this.setState({ game: {}, showDialogInfo: false });
  }

  handleOpenDialogDelete = () => {
    this.setState({ showDialogDelete: true });
  }

  handleCloseDialogDelete = () => {
    this.setState({ showDialogDelete: false });
  }

  handleChangeSelectFilterValue = (value) => {
    this.setState({ filterValue: value });
  }

  handleFilter = () => {
    const { filterValue } = this.state;
    if (filterValue.trim().length > 0) {
      this.handleSearchGames(`?status=${filterValue}`);
    } else {
      this.handleSearchGames();
    }
  }

  render() {
    const { showSpinner, games, game, filterValue } = this.state;

    return (
      <div>
        <Form inline className="form-container">
          <Button variant="success" className="without-border" onClick={() => this.handleRedirectToGame()}><BsPlus /></Button>
          <Form.Control as="select"  className="noborder-select" style={{ flex: 1 }} onChange={(e) => this.handleChangeSelectFilterValue(e.target.value)} value={filterValue}>
            <option value="">Todos</option>
            <option value="ABERTO">Aberto</option>
            <option value="EM_ANDAMENTO">Em andamento</option>
            <option value="FINALIZADO">Finalizado</option>
          </Form.Control>
          <Button variant="dark" className="without-border" onClick={() => this.handleFilter()}><BsSearch /></Button>
        </Form>
         {showSpinner ? <div className="center-spinner"><Spinner animation="border" variant="primary" /></div> : 
          games.length > 0 && <div className='d-flex flex-row justify-content-between flex-wrap' style={{ padding: '50px' }}>
            {games.map((game) =>
              <Card        
                key={game._id}
                style={{ width: '18rem' }}
                className={"mb-2 card " + getBorderFromStatus(game.status)}
                onClick={() => this.handleOpenDialogInfo(game)}
              >            
                <Card.Body>
                  <Card.Title className="card-title">
                    {game.competition.name} - {parsePtBr(game.date_game)}
                  </Card.Title>
                  <Card.Text>
                    {game.team_one.name} {game.goals_team_one} X {game.goals_team_two} {game.team_two.name}
                  </Card.Text>
                  <Card.Text>
                    {getDescriptionFromStatus(game.status)}
                  </Card.Text>
                </Card.Body>
              </Card>
            )}
          </div>
         }

         {!showSpinner && games.length === 0 && <div className="empty-list">Jogos não encontrados!</div>}

        <Dialog 
          show={this.state.showDialogDelete}
          title="Excluir jogo"
          message="Deseja realmente excluir?"
          handleNo={this.handleCloseDialogDelete}
          handleYes={this.handleDeleteItem}
        />

        <Modal
          show={this.state.showDialogInfo}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          onHide={this.handleCloseDialogInfo}
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter" className="title-description">{game.team_one && game.team_one.name} {game.goals_team_one && <b>{game.goals_team_one}</b>} x {game.goals_team_two && <b>{game.goals_team_two}</b>} {game.team_two && game.team_two.name}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <table>
              <thead></thead>
              <tbody>
                {game.competition && <tr>
                  <td className="description">Competição:</td>
                  <td>{game.competition.name}</td>
                </tr>}
                {game.date_game && <tr>
                  <td className="description">Dt. Jogo:</td>
                  <td>{parsePtBr(game.date_game)}</td>
                </tr>}
                {game.stadium && <tr>
                  <td className="description">Estádio:</td>
                  <td>{game.stadium}</td>
                </tr>}
                {game.status && <tr>
                  <td className="description">Status:</td>
                  <td>{getDescriptionFromStatus(game.status)}</td>
                </tr>}
                {game.description && <tr>
                  <td className="description">Descrição:</td>
                  <td>{game.description}</td>
                </tr>}
              </tbody>
            </table>
            <div className="container-dialog-buttons">
              <Button variant="primary" onClick={() => this.handleRedirectToGame(game._id)} className="space-btn-danger"><BsPencil /></Button>
              <Button variant="danger" onClick={() => this.handleOpenDialogDelete()} className="space-btn-danger"><BsTrash /></Button>
            </div>
          </Modal.Body>
        </Modal>
        <ToastContainer />
      </div>
    );
  }
};

export default withRouter(Games);
