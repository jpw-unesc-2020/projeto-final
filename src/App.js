import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import './App.css';

import Competitions from './containers/competitions';
import Competition from './containers/competition';
import Teams from './containers/teams';
import Team from './containers/team';
import Games from './containers/games';
import Game from './containers/game';

export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      navlink: 'games',
    };
  }

  componentDidMount() {
    const url = document.URL;
    this.setState({ navlink: url.substring(url.lastIndexOf('/') + 1, url.length) });
  }

  render() {
    const { navlink } = this.state;

    return (
      <>
        <Navbar bg="primary" variant="dark">
          <Navbar.Brand href="games"><img src="https://www.pngkit.com/png/full/28-286535_soccer-silhouette-two-players-logo-play-football-png.png" width="30" style={{ marginRight: '10px' }} />Projeto Final</Navbar.Brand>
            <Nav className="mr-auto">
              <Nav.Link href="games" active={navlink === 'games'}>Jogos</Nav.Link>
              <Nav.Link href="teams" active={navlink === 'teams'}>Times</Nav.Link>
              <Nav.Link href="competitions" active={navlink === 'competitions'}>Competições</Nav.Link>              
            </Nav>
        </Navbar>
        <BrowserRouter>
          <Switch>    
            <Route path="/games">
              <Games />
            </Route>
            <Route path="/game">
              <Game />
            </Route>
            <Route path="/competitions">
              <Competitions />
            </Route>
            <Route path="/competition">
              <Competition />
            </Route>
            <Route path="/teams">
              <Teams />
            </Route>
            <Route path="/team">
              <Team />
            </Route>
            <Route path="/">
              <Redirect to="/games" />
            </Route>
          </Switch>
        </BrowserRouter>  
      </>
    );
  }
};
