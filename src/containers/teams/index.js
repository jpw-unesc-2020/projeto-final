import React from 'react';
import { withRouter } from 'react-router-dom';
import { Button, Table, Form, FormControl, Modal, Spinner } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { BsTrash, BsPencil, BsSearch, BsEye, BsPlus } from "react-icons/bs";
import { GET, DELETE, STATUS_CODE } from '../../api';
import { parsePtBr } from '../../utils/date';
import './index.css';

import Dialog from '../../components/modal';

class Teams extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      showDialogDelete: false,
      showDialogInfo: false,
      showSpinner: true,
      filterValue: '',
      teams: [],
      team: {},
    };
  }

  componentDidMount() {
    this.handleSearchTeams();
  }

  handleSearchTeams = (filter = '') => {
    this.setState({ showSpinner: true });

    GET(`/team${filter}`).then(res => {
      this.setState({ teams: res.data, showSpinner: false });
    }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
  }

  handleRedirectToTeam = (_id) => {
    this.props.history.push('team',_id);
  }

  handleDeleteItem = () => {
    const { team } = this.state;

    DELETE(`/team/${team._id}`).then(res => {
      if (res.status === STATUS_CODE.OK) {
        toast.success(`Time ${team.name.toLowerCase()} excluído com sucesso!`, { position: 'top-right', autoClose: 2000, closeOnClick: false, pauseOnHover: false, draggable: false });
        this.handleSearchTeams();
        this.handleCloseDialogDeleteItem();
      } else if (res.status === STATUS_CODE.NO_CONTENT) {
        toast.error(`Time ${team.name.toLowerCase()} vinculado a uma partida, por isso não é possível realizar sua exclusão!`, { position: 'top-right', autoClose: 4000, closeOnClick: false, pauseOnHover: false, draggable: false });
      }
    }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
  }

  handleOpenDialogDeleteItem = (team) => {
    this.setState({ team: team, showDialogDelete: true });
  }

  handleCloseDialogDeleteItem = () => {
    this.setState({ team: {}, showDialogDelete: false });
  }

  handleOpenDialogInfo = (team) => {
    this.setState({ team: team, showDialogInfo: true });   
  }

  handleCloseDialogInfo = () => {
    this.setState({ team: {}, showDialogInfo: false })
  }

  handleChangeSelectFilterValue = (value) => {
    this.setState({ filterValue: value });
  }

  handleFilter = () => {
    const { filterValue } = this.state;
    if (filterValue.trim().length > 0) {
      this.handleSearchTeams(`?name=${filterValue}&stadium=${filterValue}&sponsor_master=${filterValue}&location=${filterValue}`);
    } else {
      this.handleSearchTeams();
    }
  }

  onKeyDownHandler = e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      e.stopPropagation();
      this.handleFilter();
    }
  }

  render() {
    const { showSpinner, teams, team, filterValue } = this.state;

    return (
      <div>
        <Form inline>
          <Button variant="success" className="without-border" onClick={() => this.handleRedirectToTeam()}><BsPlus /></Button>
          <FormControl type="text" className="without-border without-outline-input" placeholder="Buscar" style={{ flex: 1 }} onChange={(e) => this.handleChangeSelectFilterValue(e.target.value)} value={filterValue} onKeyDown={this.onKeyDownHandler}/>
          <Button variant="dark" className="without-border" onClick={() => this.handleFilter()}><BsSearch /></Button>
        </Form>
        {showSpinner ? <div className="center-spinner"><Spinner animation="border" variant="primary" /></div> :
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Nome</th>
                <th>Estádio</th>
                <th>Patrocinador</th>
                <th>Localização</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {teams.length > 0 && teams.map((team) =>
                <tr key={team._id}>
                  <td>{team.name}</td>
                  <td>{team.stadium}</td>
                  <td>{team.sponsor_master}</td>
                  <td>{team.location}</td>
                  <td className="column-options">
                    <Button variant="info" onClick={() => this.handleOpenDialogInfo(team)} className="space-btn-danger"><BsEye /></Button>
                    <Button variant="primary" onClick={() => this.handleRedirectToTeam(team._id)} className="space-btn-danger"><BsPencil /></Button>
                    <Button variant="danger" onClick={() => this.handleOpenDialogDeleteItem(team)} className="space-btn-danger"><BsTrash /></Button>
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
        }
        
        {!showSpinner && teams.length === 0 && <div className="empty-list">Times não encontrados!</div>}
        
        <Dialog 
          show={this.state.showDialogDelete}
          title="Excluir time"
          message="Deseja realmente excluir?"
          handleNo={this.handleCloseDialogDeleteItem}
          handleYes={this.handleDeleteItem}
        />

        <Modal
          show={this.state.showDialogInfo}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          onHide={this.handleCloseDialogInfo}
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter" className="title-description"><b>{team.name}</b></Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <table>
              <thead></thead>
              <tbody>
                {team.nickname && <tr>
                  <td className="description">Apelido:</td>
                  <td>{team.nickname}</td>
                </tr>}
                {team.stadium && <tr>
                  <td className="description">Estádio:</td>
                  <td>{team.stadium}</td>
                </tr>}
                {team.mascot && <tr>
                  <td className="description">Mascote:</td>
                  <td>{team.mascot}</td>
                </tr>}
                {team.date_foundation && <tr>
                  <td className="description">Dt. Fundação:</td>
                  <td>{parsePtBr(team.date_foundation, false)}</td>
                </tr>}
                {team.sponsor_master && <tr>
                  <td className="description">Patrocinador:</td>
                  <td>{team.sponsor_master}</td>
                </tr>}
                {team.location && <tr>
                  <td className="description">Localização:</td>
                  <td>{team.location}</td>
                </tr>}
                {team.number_fans && <tr>
                  <td className="description">N° Torcedores:</td>
                  <td>{team.number_fans}</td>
                </tr>}
              </tbody>
            </table>  
          </Modal.Body>
        </Modal>
        <ToastContainer />
      </div>
    );
  }
};

export default withRouter(Teams);
