import React from 'react';
import { Button, Form } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { withRouter } from 'react-router-dom';
import { GET, POST, PUT, STATUS_CODE } from '../../api';
import { parseDateTimeToField } from '../../utils/date';
import './index.css';

class Game extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      validated: false,
      formDisabled: false,
      game: {},
      teams: [],
      competitions: [],
    }
  }

  componentDidMount() {
    const { location } = this.props;
    
    if (location && location.state) {
      GET(`/game/${location.state}`).then(res => {
        this.setState({ game: res.data });
      }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
    }

    GET(`/team`).then(res => {
      this.setState({ teams: res.data });
    }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));

    GET(`/competition`).then(res => {
      this.setState({ competitions: res.data });
    }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
  }

  onChangeInfo = (field, value) => {
    this.setState({ game: {...this.state.game, [field]: value} });    
  }

  handleSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();

    const form = event.currentTarget;
    let redirect = true;

    if (form.checkValidity() === false) {
      redirect = false;
    }

    this.setState({ validated: true });

    if (redirect) {
      const { game } = this.state;

      if (game._id) {
        PUT(`/game/${game._id}`, game).then(res => {
          if (res.status === STATUS_CODE.OK) {
            this.handleToastSaveSuccess();
          } else {
            this.handleToastSaveError();
          }
        }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
      } else {
        POST(`/game`, game).then(res => {
          console.log(res);
          if (res.status === STATUS_CODE.OK) {
            this.handleToastSaveSuccess();
          } else {
            this.handleToastSaveError();
          }
        }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
      }
    }
  }

  handleToastSaveSuccess = () => {
    this.setState({ formDisabled: true });
    toast.success('Jogo salvo com sucesso!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false });
    setTimeout(() => {this.props.history.push('games')}, 3000);
  }

  handleToastSaveError = () => {
    toast.error('Erro ao salvar jogo!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false });
  }

  render() {
    const { validated, game, teams, competitions, formDisabled } = this.state;
    
    return (
      <div className="container">
        <div className="content">
          <div className="title">Jogo</div>
          <Form noValidate validated={validated} onSubmit={this.handleSubmit}>
            <div className="small-screen flex-row">
              <Form.Group controlId="uid" className="container-field">
                <Form.Label>UID</Form.Label>
                <Form.Control type="text" disabled value={game._id}/>
              </Form.Group>
              <Form.Group controlId="team_one" className="container-field">
                <Form.Label>Time 1<span className="required">*</span></Form.Label>
                <Form.Control required as="select" onChange={(e) => this.onChangeInfo('team_one', e.target.value)} value={game.team_one ? game.team_one._id : ''} disabled={game._id || formDisabled}>
                  <option value="">Selecione um time</option>
                  {teams.map((team) => <option key={team._id} value={team._id}>{team.name}</option>)}
                </Form.Control>  
                <Form.Control.Feedback type="invalid">Time 1 obrigatório!</Form.Control.Feedback>
              </Form.Group>
              <div className="container-goals">
                <Form.Group controlId="goals_team_one" className="container-field value-goals">
                  <Form.Label>Gols Time 1{game.status === 'FINALIZADO' && <span className="required">*</span>}</Form.Label>
                  <Form.Control type="number" required={game.status === 'FINALIZADO'} onChange={(e) => this.onChangeInfo('goals_team_one', e.target.value)} style={{ textAlign: 'right' }} value={game.goals_team_one} disabled={formDisabled}/>
                  <Form.Control.Feedback type="invalid">Gols time 1 obrigatório!</Form.Control.Feedback>
                </Form.Group>
                <div className="description-goals">X</div>
                <Form.Group controlId="goals_team_two" className="container-field value-goals">
                  <Form.Label>Gols Time 2{game.status === 'FINALIZADO' && <span className="required">*</span>}</Form.Label>
                  <Form.Control type="number" required={game.status === 'FINALIZADO'} onChange={(e) => this.onChangeInfo('goals_team_two', e.target.value)} value={game.goals_team_two} disabled={formDisabled}/>
                  <Form.Control.Feedback type="invalid">Gols time 2 obrigatório!</Form.Control.Feedback>
                </Form.Group>
              </div>
              <Form.Group controlId="team_two" className="container-field">
                <Form.Label>Time 2<span className="required">*</span></Form.Label>
                <Form.Control required as="select" onChange={(e) => this.onChangeInfo('team_two', e.target.value)} value={game.team_two ? game.team_two._id : ''} disabled={game._id || formDisabled}>
                  <option value="">Selecione um time</option>
                  {teams.map((team) => <option key={team._id} value={team._id}>{team.name}</option>)}
                </Form.Control>
                <Form.Control.Feedback type="invalid">Time 2 obrigatório!</Form.Control.Feedback>
              </Form.Group>
            </div>
            <div className="small-screen flex-row">
              <Form.Group controlId="competition" className="container-field">
                <Form.Label>Competição<span className="required">*</span></Form.Label>
                <Form.Control required as="select" onChange={(e) => this.onChangeInfo('competition', e.target.value)} value={game.competition ? game.competition._id : ''} disabled={game._id || formDisabled}>
                  <option value="">Selecione uma competição</option>
                  {competitions.map((competition) => <option key={competition._id} value={competition._id}>{competition.name}</option>)}
                </Form.Control>  
                <Form.Control.Feedback type="invalid">Competição obrigatório!</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="date_game" className="container-field">
                <Form.Label>Dt. Jogo<span className="required">*</span></Form.Label>
                <Form.Control required type="datetime-local" onChange={(e) => this.onChangeInfo('date_game', e.target.value)} value={parseDateTimeToField(game.date_game)} disabled={game._id || formDisabled}/>
                <Form.Control.Feedback type="invalid">Dt. Jogo obrigatório!</Form.Control.Feedback>
              </Form.Group>
            </div>
            <div className="small-screen flex-row">
              <Form.Group controlId="stadium" className="container-field">
                <Form.Label>Estádio<span className="required">*</span></Form.Label>
                <Form.Control required type="text" onChange={(e) => this.onChangeInfo('stadium', e.target.value)} value={game.stadium} disabled={game._id || formDisabled}/>
                <Form.Control.Feedback type="invalid">Estádio obrigatório!</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="status" className="container-field">
                <Form.Label>Status<span className="required">*</span></Form.Label>
                <Form.Control required as="select" onChange={(e) => this.onChangeInfo('status', e.target.value)} value={game.status} disabled={formDisabled}>
                  {!game._id && <option value="">Selecione um status</option>}
                  <option value="ABERTO">Aberto</option>
                  <option value="EM_ANDAMENTO">Em andamento</option>
                  <option value="FINALIZADO">Finalizado</option>
                </Form.Control>  
                <Form.Control.Feedback type="invalid">Status obrigatório!</Form.Control.Feedback>
              </Form.Group>
            </div>
            <div className="small-screen flex-row">
              <Form.Group controlId="description" className="container-field">
                <Form.Label>Descrição</Form.Label>
                <Form.Control type="text" onChange={(e) => this.onChangeInfo('description', e.target.value)} value={game.description} disabled={game._id || formDisabled}/>
                <Form.Control.Feedback type="invalid">Descrição obrigatória!</Form.Control.Feedback>
              </Form.Group>
            </div>
            <div className="container-button">
              <Button variant="secondary" type="button" disabled={formDisabled} onClick={() => this.props.history.push('games')} style={{ marginRight: '15px'}}>Cancelar</Button>
              <Button variant="success" type="submit" disabled={formDisabled}>Salvar</Button>
            </div>
          </Form>  
        </div>
        <ToastContainer />
      </div>
    );
  }
};

export default withRouter(Game);
