import React from 'react';
import { withRouter } from 'react-router-dom';
import { Button, Table, Form, FormControl, Modal, Spinner } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { BsTrash, BsPencil, BsSearch, BsEye, BsPlus } from "react-icons/bs";
import { GET, DELETE, STATUS_CODE } from '../../api';
import { parsePtBr } from '../../utils/date';
import './index.css';

import Dialog from '../../components/modal';

class Competitions extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      showDialogDelete: false,
      showDialogInfo: false,
      showSpinner: true,
      filterValue: '',
      competitions: [],
      competition: {},
    };
  }

  componentDidMount() {
    this.handleSearchCompetitions();
  }

  handleSearchCompetitions = (url = '') => {
    this.setState({ showSpinner: true });
    GET(`/competition${url}`).then(res => {
      this.setState({ competitions: res.data, showSpinner: false });
    }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
  }

  handleRedirectToCompetition = (_id) => {
    this.props.history.push('competition', (_id ? _id : ''));
  }

  handleDeleteItem = () => {
    const { competition } = this.state;

    DELETE(`/competition/${competition._id}`).then(res => {
      if (res.status === STATUS_CODE.OK) {
        toast.success(`Competição ${competition.name.toLowerCase()} excluída com sucesso!`, { position: 'top-right', autoClose: 2000, closeOnClick: false, pauseOnHover: false, draggable: false });
        this.handleSearchCompetitions();
        this.handleCloseDialogDeleteItem();
      } else if (res.status === STATUS_CODE.NO_CONTENT) {
        toast.error(`Competição ${competition.name.toLowerCase()} vinculada a uma partida, por isso não é possível realizar sua exclusão!`, { position: 'top-right', autoClose: 4000, closeOnClick: false, pauseOnHover: false, draggable: false });
      }
    }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));  
  }

  handleOpenDialogDeleteItem = (competition) => {
    this.setState({ competition: competition, showDialogDelete: true });
  }

  handleCloseDialogDeleteItem = () => {
    this.setState({ competition: {}, showDialogDelete: false });
  }

  handleOpenDialogInfo = (competition) => {
    this.setState({ competition: competition, showDialogInfo: true });   
  }

  handleCloseDialogInfo = () => {
    this.setState({ competition: {}, showDialogInfo: false });
  }
  
  handleChangeSelectFilterValue = (value) => {
    this.setState({ filterValue: value });
  }

  handleFilter = () => {
    const { filterValue } = this.state;
    if (filterValue.trim().length > 0) {
      this.handleSearchCompetitions(`?name=${filterValue}&organization=${filterValue}&location=${filterValue}`);
    } else {
      this.handleSearchCompetitions();
    }
  }

  onKeyDownHandler = e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      e.stopPropagation();
      this.handleFilter();
    }
  }

  render() {
    const { showSpinner, competitions, competition, filterValue } = this.state;

    return (
      <div>
        <Form inline>
          <Button variant="success" className="without-border" onClick={() => this.handleRedirectToCompetition()}><BsPlus /></Button>
          <FormControl type="text" className="without-border without-outline-input" placeholder="Buscar" style={{ flex: 1 }} onChange={(e) => this.handleChangeSelectFilterValue(e.target.value)} value={filterValue} onKeyDown={this.onKeyDownHandler}/>
          <Button variant="dark" className="without-border" onClick={() => this.handleFilter()}><BsSearch /></Button>
        </Form>
        {showSpinner ? <div className="center-spinner"><Spinner animation="border" variant="primary" /></div> : 
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Nome</th>
                <th>Organização</th>
                <th>Localização</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {competitions.length > 0 && competitions.map((competition) =>
                <tr key={competition._id}>
                  <td>{competition.name}</td>
                  <td>{competition.organization}</td>
                  <td>{competition.location}</td>
                  <td className="column-options">
                    <Button variant="info" onClick={() => this.handleOpenDialogInfo(competition)} className="space-btn-danger"><BsEye /></Button>
                    <Button variant="primary" onClick={() => this.handleRedirectToCompetition(competition._id)} className="space-btn-danger"><BsPencil /></Button>
                    <Button variant="danger" onClick={() => this.handleOpenDialogDeleteItem(competition)} className="space-btn-danger"><BsTrash /></Button>
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
        }

        {!showSpinner && competitions.length === 0 && <div className="empty-list">Competições não encontradas!</div>}

        <Dialog 
          show={this.state.showDialogDelete}
          title="Excluir competição"
          message="Deseja realmente excluir?"
          handleNo={this.handleCloseDialogDeleteItem}
          handleYes={this.handleDeleteItem}
        />

        <Modal
          show={this.state.showDialogInfo}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          onHide={this.handleCloseDialogInfo}
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter" className="title-description"><b>{competition.name}</b></Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <table>
              <thead></thead>
              <tbody>
                {competition.organization && <tr>
                  <td className="description">Organização:</td>
                  <td>{competition.organization}</td> 
                </tr>}
                {competition.edition && <tr>
                  <td className="description">Edição:</td>
                  <td>{competition.edition}</td>
                </tr>}
                {competition.system && <tr>
                  <td className="description">Sistema:</td>
                  <td>{competition.system}</td>
                </tr>}
                {competition.location && <tr>
                  <td className="description">Localização:</td>
                  <td>{competition.location}</td>
                </tr>}
                {competition.date_initial && <tr>
                  <td className="description">Dt. Inicial:</td>
                  <td>{parsePtBr(competition.date_initial, false)}</td>
                </tr>}
                {competition.date_end && <tr>
                  <td className="description">Dt. Final:</td>
                  <td>{parsePtBr(competition.date_end, false)}</td>
                </tr>}
                {competition.number_teams && <tr>
                  <td className="description">N° Times:</td>
                  <td>{competition.number_teams}</td>
                </tr>}
                {competition.premium_value && <tr>
                  <td className="description">Premiação:</td>
                  <td>{competition.premium_value}</td>
                </tr>}
              </tbody>
            </table>
          </Modal.Body>
        </Modal>
        <ToastContainer />
      </div>
    );
  }
};

export default withRouter(Competitions);