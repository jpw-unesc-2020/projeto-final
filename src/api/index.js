import axios from 'axios';

const URL = 'http://localhost:3005';

const CONFIG = {
  headers: { Authorization: 'Bearer db5fb842672bb7fc1a105cf3ff645e861be2e05853a73cc022544c6c20649e57' }
};

const STATUS_CODE = {
  OK: 200,
  NO_CONTENT: 204,
}

const GET = (path) => {
  return axios.get(URL + path, CONFIG);
};

const POST = (path, data) => {
  return axios.post(URL + path, data, CONFIG);
};

const PUT = (path, data) => {
  return axios.put(URL + path, data, CONFIG);
};

const DELETE = (path) => {
  return axios.delete(URL + path, CONFIG);
};

export {
  GET,
  POST,
  PUT,
  DELETE,
  STATUS_CODE
}

