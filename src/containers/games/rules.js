export const getBorderFromStatus = (status) => {
  switch(status) {
    case 'ABERTO': return 'status-aberto';
    case 'EM_ANDAMENTO': return 'status-em-andamento';
    case 'FINALIZADO': return 'status-finalizado';
  };
};

export const getDescriptionFromStatus = (status) => {
  switch(status) {
    case 'ABERTO': return 'Aberto';
    case 'EM_ANDAMENTO': return 'Em andamento';
    case 'FINALIZADO': return 'Finalizado';
  };
};