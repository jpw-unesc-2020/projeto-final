import React from 'react';
import { Button, Modal } from 'react-bootstrap';

export default class Dialog extends React.Component {
  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.handleNo}>
        <Modal.Header closeButton>
            <Modal.Title>{this.props.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{this.props.message}</Modal.Body>
        <Modal.Footer>
          <Button variant="outline-primary" onClick={this.props.handleNo}>
            {this.props.no || 'Não'}
          </Button>
          <Button variant="outline-danger" onClick={this.props.handleYes}>
            {this.props.yes || 'Sim'}
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
};
