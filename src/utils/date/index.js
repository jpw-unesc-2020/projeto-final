import moment from 'moment';

export const parsePtBr = (date, schedule = true) => {
  return moment(date).format(('DD/MM/YYYY' + (schedule ? ' HH:MM' : '')));
};

export const parseToField = (date) => {
  if (!date) return undefined;

  return moment(date).format('YYYY-MM-DD');
};

export const parseDateTimeToField = (date) => {
  if (!date) return undefined;

  return moment(date).format('YYYY-MM-DDTkk:mm');
};