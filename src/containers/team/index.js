import React from 'react';
import { Button, Form } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import { withRouter } from 'react-router-dom';
import { GET, POST, PUT, STATUS_CODE } from '../../api';
import { parseToField } from '../../utils/date';
import './index.css';

class Team extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      validated: false,
      formDisabled: false,
      team: {},
    }
  }

  componentDidMount() {
    const { location } = this.props;
    
    if (location && location.state) {
      GET(`/team/${location.state}`).then(res => {
        this.setState({ team: res.data });
      }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
    }

  }

  onChangeInfo = (field, value) => {
    this.setState({ team: {...this.state.team, [field]: value} });    
  }

  handleSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();

    const form = event.currentTarget;
    let redirect = true;

    if (form.checkValidity() === false) {
      redirect = false;
    }

    this.setState({ validated: true });

    if (redirect) {
      const { team } = this.state;

      if (team._id) {
        PUT(`/team/${team._id}`, team).then(res => {
          if (res.status === STATUS_CODE.OK) {
            this.handleToastSaveSuccess();
          } else {
            this.handleToastSaveError();
          }
        }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
      } else {
        POST(`/team`, team).then(res => {
          if (res.status === STATUS_CODE.OK) {
            this.handleToastSaveSuccess();
          } else {
            this.handleToastSaveError();
          }
        }).catch(() => toast.error('Erro ao acessar recurso! Contate o responsável pela aplicação!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false }));
      }
    }
  }

  handleToastSaveSuccess = () => {
    this.setState({ formDisabled: true });
    toast.success('Time salvo com sucesso!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false });
    setTimeout(() => {this.props.history.push('teams')}, 3000);
  }

  handleToastSaveError = () => {
    toast.error('Erro ao salvar time!', { position: 'top-right', autoClose: 3000, closeOnClick: false, pauseOnHover: false, draggable: false });
  }

  render() {
    const { validated, team, formDisabled } = this.state;
    return (
      <div className="container">        
        <div className="content">
          <div className="title">Time</div>
          <Form noValidate validated={validated} onSubmit={this.handleSubmit} disabled>
            <div className="small-screen flex-row">
              <Form.Group controlId="uid" className="container-field">
                <Form.Label>UID</Form.Label>
                <Form.Control type="text" disabled value={team._id}/>
              </Form.Group>
              <Form.Group controlId="name" className="container-field">
                <Form.Label>Nome<span className="required">*</span></Form.Label>
                <Form.Control required type="text" onChange={(e) => this.onChangeInfo('name', e.target.value)} value={team.name} disabled={formDisabled} />
                <Form.Control.Feedback type="invalid">Nome obrigatório!</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="nickname" className="container-field">
                <Form.Label>Apelido<span className="required">*</span></Form.Label>
                <Form.Control required type="text" onChange={(e) => this.onChangeInfo('nickname', e.target.value)} value={team.nickname} disabled={formDisabled}/>
                <Form.Control.Feedback type="invalid">Apelido obrigatório!</Form.Control.Feedback>
              </Form.Group>
            </div>
            <div className="small-screen flex-row">
              <Form.Group controlId="stadium" className="container-field">
                <Form.Label>Estádio<span className="required">*</span></Form.Label>
                <Form.Control required type="text" onChange={(e) => this.onChangeInfo('stadium', e.target.value)} value={team.stadium} disabled={formDisabled}/>
                <Form.Control.Feedback type="invalid">Estádio obrigatório!</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="mascot" className="container-field">
                <Form.Label>Mascote</Form.Label>
                <Form.Control type="text" onChange={(e) => this.onChangeInfo('mascot', e.target.value)} value={team.mascot} disabled={formDisabled}/>
              </Form.Group>
            </div>
            <div className="small-screen flex-row">
              <Form.Group controlId="date_foundation" className="container-field">
                <Form.Label>Dt. Fundação<span className="required">*</span></Form.Label>
                <Form.Control required type="date" onChange={(e) => this.onChangeInfo('date_foundation', e.target.value)} value={parseToField(team.date_foundation)} disabled={formDisabled}/>
                <Form.Control.Feedback type="invalid">Data de fundação obrigatório!</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="sponsor_master" className="container-field">
                <Form.Label>Patrocinador Master</Form.Label>
                <Form.Control type="text" onChange={(e) => this.onChangeInfo('sponsor_master', e.target.value)} value={team.sponsor_master} disabled={formDisabled}/>
              </Form.Group>
            </div>
            <div className="small-screen flex-row">
              <Form.Group controlId="location" className="container-field">
                <Form.Label>Localização<span className="required">*</span></Form.Label>
                <Form.Control required type="text" onChange={(e) => this.onChangeInfo('location', e.target.value)} value={team.location} disabled={formDisabled}/>
                <Form.Control.Feedback type="invalid">Localização obrigatório!</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="number_fans" className="container-field">
                <Form.Label>N° Torcedores<span className="required">*</span></Form.Label>
                <Form.Control required type="number" onChange={(e) => this.onChangeInfo('number_fans', e.target.value)} value={team.number_fans} disabled={formDisabled}/>
                <Form.Control.Feedback type="invalid">N° de torcedores obrigatório!</Form.Control.Feedback>
              </Form.Group>
            </div>
            <div className="container-button">
              <Button variant="secondary" type="button" disabled={formDisabled} onClick={() => this.props.history.push('teams')} style={{ marginRight: '15px'}}>Cancelar</Button>
              <Button variant="success" type="submit" disabled={formDisabled}>Salvar</Button>
            </div>
          </Form>  
        </div>
        <ToastContainer />
      </div>
    );
  }
};

export default withRouter(Team);
